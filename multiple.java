import java.util.Scanner;

class Multiple{
    public static void main(String[] args) {
        System.out.println("enter the number");
        //taking the input using scanner class
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
         System.out.println(calc(num));
    }
    public static int calc(int num) {
    //intializing the sum with 0
    int sum=0;
    
    for(int i=0; i<num;i++){
    // if it is multiple of 3 or 5 or both "||" includes all the cases
    if(i%3==0||i%5==0){
        // adding the number into sum
        sum =sum+i;
    }
    }
    return sum;
    }
}