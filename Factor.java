import java.util.Scanner;
public class Factor {
    public static void main(String[] args) {
        long temp = 600851475143l;
        Scanner sc = new Scanner(System.in);
        temp = sc.nextLong();
        // as the number is out of range for integer it has to be long
        System.out.println(maxPrimeFactors(temp));
    }
	static long maxPrimeFactors(long n)
	{
		long maxPrime = -1;

		// removing 2 & 3 from the factor of n
		// that divide n
		while (n % 2 == 0) {
			maxPrime = 2;
			//shifting the bit by 1 to right 
			// equivalent to dividing by 2
			n = n >> 1;
		}
		while (n % 3 == 0) {
			maxPrime = 3;
			n = n / 3;
		}
		
		// when all the 2 & 3 will be removed from the nuumber as factor

		for (int i = 5; i <= Math.sqrt(n); i += 2) {
			while (n % i == 0) {
				maxPrime = i;
				n = n / i;
			}
		}
		// This condition is to handle the case
		// when n is a prime number greater than 4
		if (n > 4)
			maxPrime = n;
		return maxPrime;
	}
}